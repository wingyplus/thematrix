Mix.install([:yaml_elixir, :ymlr])

defmodule Program do
  def run([path, env, destination]) do
    service_deployments = YamlElixir.read_from_file!(path)

    yaml =
      service_deployments
      |> Enum.filter(&deploy_to_target?(&1, env))
      |> Enum.into(%{}, &to_gitlab_pipeline/1)
      |> Map.put(:workflow, %{name: "Deploy..."})
      |> Ymlr.document!()

    File.write!(destination, String.trim_leading(yaml, "---\n"))
  end

  defp deploy_to_target?(deployment, env) do
    env in deployment["target-envs"]
  end

  defp to_gitlab_pipeline(deployment) do
    {
      deployment["service-name"],
      %{
        script: [
          "echo 'Job from #{deployment["service-name"]}'"
        ]
      }
    }
  end
end

Program.run(System.argv())
